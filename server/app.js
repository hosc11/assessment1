/**
 * Server side code.
 */
"use strict";
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
var fs = require('fs');
var registrationData;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

app.use(express.static(__dirname + "/../client/"));

var country = {};
fs.readFile(__dirname + '/country.json', 'utf8',function(err, data) {
    if(err) throw err;
    country = JSON.parse(data);

});

app.get("/countrylist", function (req, res) {
    //console.log(">>>>>in app.get countrylist");
    //console.log(country);
    res.json(country);
});

app.post("/users", function(req, res) {
    console.log("Received user obejct " + req.body);
    console.log("Received user obejct " + JSON.stringify(req.body));
    var registrationData = req.body;
    
    console.log(">>>registrationData.selcountry >>>" + registrationData.selcountry);
    /*
    if (registrationData.selcountry == "") {
        registrationData.selcountry == "SG";
    }
    */
    res.status(200).json(registrationData);
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});