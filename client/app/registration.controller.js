(function () {
    "use strict";
    angular.module("RegistrationApp").controller("RegistrationCtrl", RegistrationCtrl);
    
    RegistrationCtrl.$inject = ["$http"];

    function RegistrationCtrl($http) {
        var self = this;
        self.ageValidate = false;
        self.loadRegistration = false;
        self.submitted = false;
        self.defaultCountry = {"name": "Singapore", "code": "SG"};
        
        var minAge = 18;
        
        self.validateMessage = "";
        self.registrationMessage = "";

        self.user = {
            email: "",
            password: "",
            name: "",
            gender: "",
            dateOfBirth: "",
            address:"",
            selcountry:"",
            contactNumber:""
        };

        self.initForm = function () {   
            $http.get("/countrylist")
                .then(function (result) {
                    //console.log(">>>>in initForm");
                    //console.log(result);
                    self.country = result.data;
                }).catch(function (e) {
                    console.log(e);
                });
        };

        self.initForm();

        self.ageValidation = function(birthday) {
            //console.log(">>> in ageValidation function");
            //console.log("birthday >>>" + birthday);
            if(birthday != "") {
                var ageDifMs = Date.now() - birthday.getTime();
                var ageDate = new Date(ageDifMs); // miliseconds from epoch
                //console.log("ageDifMs >> " + ageDifMs);
                //console.log("ageDate >> " + ageDate);
                
                self.userAge = Math.abs(ageDate.getUTCFullYear() - 1970);
                //self.userAge = ageDate.getUTCFullYear() - 1970;
                //console.log("userAge >> " + self.userAge);
                
                if (self.userAge >= minAge) {
                    self.ageValidate = true;
                    return true;
                }else {
                    self.ageValidate = false;
                    self.validateMessage = "I am sorry! The minimum age for this registration is " + minAge + " years old."
                    return false;
                }
                 
            }
        };
        self.loadRegistrationContent = function (){
            //console.log(">>>> in loadRegistrationContent function >>");
            //console.log("self.loadRegistration >>" + self.loadRegistration);
            self.loadRegistration = true;
        };

        self.registerUser = function() {
            //console.log(self.user.email);
            //console.log(self.user.password);
            $http.post("/users", self.user)
                .then(function(result) {
                    console.log(result);
                    self.submitted = true;
                    self.registrationMessage = "Welcome! " + result.data.name;
                    $('#myModal').modal('show');
                    
                }).catch(function(e) {
                    console.log(e);
                });
        };

    };

}) ();